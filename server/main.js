import { Meteor } from 'meteor/meteor';

ArrUnique = function (value, index, self) { 
    return self.indexOf(value) === index;
}
Meteor.startup(() => {

  Meteor.methods({
    getPriceDetails: function (assetNames, duration) {

      // check for args
      if( !assetNames || !assetNames.trim() || !duration || !duration.start || !duration.end ){

        console.log('Both asset name and duration are compulsory.');
        throw new Meteor.Error(404,"Both asset name and duration are compulsory.");
        return;

      }

      var start = duration.start;
      var end = duration.end;

      //checking dates are in MM/DD/YY HH:mm format
      if( moment(start, "MM/DD/YY HH:mm").format('MM/DD/YY HH:mm') !== start ||
          moment(end, "MM/DD/YY HH:mm").format('MM/DD/YY HH:mm') !== end ){

        throw new Meteor.Error(404,"Dates should be in MM/DD/YY HH:mm format");
        return;

      }

      // If start is after end throws error
      if( moment(start, "MM/DD/YY HH:mm").isAfter( moment(end, "MM/DD/YY HH:mm") )  ){

        throw new Meteor.Error(404,"Start time should be before end time.");
        return;
      }

      // If end is after now
      if( moment().isBefore( moment(end, "MM/DD/YY HH:mm") )  ){

        throw new Meteor.Error(404,"End time should be before current time.");
        return;
      }

      assetNames = assetNames.split(",");
      // get pricing from rest end point
      var res = getPrice(assetNames, duration);

      if( res.name === 'Error'){

        throw new Meteor.Error(404,"We don't have information about "+res.names+". Please edit input and resubmit");
        return;
      }else{
        return res.name+" is the highest gainer with "+res.gain+"%";    
      }
      

    }
  });

  var getPrice = function(assetNames, duration){

    // Rest call
    
    var startData = [], endData = [];

    var hasError = [];

    assetNames.forEach(function (a_name) {
      
      var startUnix = moment(duration.start, "MM/DD/YY HH:mm").unix();

      var startURL = "https://www.cryptocompare.com/api/data/pricehistorical?fsym="+a_name+"&tsyms=USD&ts="+startUnix;
      var start_resp = HTTP.get(startURL);
      // console.log(start_resp);

      var startPrice = undefined;

      if( start_resp.statusCode === 200 ){

        var start_content = start_resp.content;

        start_content = JSON.parse(start_content);

        if( start_content && start_content.Data && start_content.Data[0]){

          startPrice = Number(start_content.Data[0].Price);
        }else{
          hasError.push(a_name);
        }
      }else{
        hasError.push(a_name);
      }
      
      var endUnix = moment(duration.end, "MM/DD/YY HH:mm").unix();

      var startURL = "https://www.cryptocompare.com/api/data/pricehistorical?fsym="+a_name+"&tsyms=USD&ts="+endUnix;
      var end_resp = HTTP.get(startURL);
      // console.log(end_resp);
      
      var endPrice = undefined;

      if( end_resp.statusCode === 200 ){

        var end_content = end_resp.content;
        end_content = JSON.parse(end_content);
        if( end_content && end_content.Data && end_content.Data[0]){

          endPrice = Number(end_content.Data[0].Price);
        }else{
          hasError.push(a_name);
        }
      }else{
        hasError.push(a_name);
      }


      if( startPrice && endPrice){
        
        startData.push({
          name: a_name,
          price: startPrice
        })

        endData.push({
          name: a_name,
          price: endPrice
        })
      }
    });
    
    
    /*

    // Generating random values
    
    assetNames.forEach(function (assetName) {
      
      var obj = {};
      obj.name = assetName;
      obj.gain = Math.floor(Math.random() * 10) + 1;

      startData.push(obj);

    });

    
    assetNames.forEach(function (assetName) {
      
      var obj = {};
      obj.name = assetName;
      obj.gain = Math.floor(Math.random() * 10) + 1;

      endData.push(obj);

    });
    */
    if( hasError && hasError.length > 0){

      var names = hasError.filter(ArrUnique);
      names = names.join(", ");
      return{
        name: 'Error',
        names: names
      }
    }
    var allData = [];

    assetNames.forEach(function (name) {
      
      var startObj = startData.find(function(l){
        return l.name === name;
      })

      var endObj = endData.find(function(l){
        return l.name === name;
      })

      var val = endObj.price - startObj.price;

      var gain = val/startObj.price;

      gain = Number(gain * 100).toFixed(3);

      console.log(name, "Changed from", startObj.price, " to ", endObj.price, " by ", gain);
      var newObj = {
        name: name,
        gain: gain 
      }

      allData.push(newObj)
    });

    function compare(a,b) {
      if (a.gain > b.gain)
        return -1;
      if (a.gain < b.gain)
        return 1;
      return 0;
    }

    var sortedObjs = allData.sort(compare);

    return sortedObjs[0];

  }
});
