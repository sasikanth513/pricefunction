import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import './main.html';

Template.hello.onCreated(function helloOnCreated() {
});

Template.hello.helpers({
 
});

Template.hello.events({
  'click button':function(event, instance) {
    let assetNames = $("#assetNames").val();
    let startDate = $("#startDate").val();
    let endDate = $("#endDate").val();

    if( !assetNames || !assetNames.trim() || !startDate || !endDate ){
      alert("Invalid values");
      return;
    }
    // increment the counter when button is clicked
    Meteor.call('getPriceDetails',assetNames, { start: startDate, end: endDate }, function (error, result) {
      if(error){
        alert(error.reason);
      }else{
        alert(result)
      }
    });
  },
});
